Test Daemons
------------

This package provides a series of pre-configured environments for
running test suites against network daemons on a Debian system.

It is intended for use/integration with autopkgtest and related CI,
without incurring any external network activity.

It was originally sketched out over on https://bugs.debian.org/940461
like so:

 * test-suite-ca

    a package that sets up and configures a certificate authority, and
    loads its public key in the standard ca-certificates location.  It
    permits automated registration of domain names within .test

 * test-suite-dns-daemon

    a package that provides recursive DNS resolution for the local
    system, with a lookaside lookup for everything in .test (see
    https://tools.ietf.org/html/rfc6761#section-6.2) and a way to bind
    addresses within the .test TLD.

 * test-suite-network

    a package that sets up a dummy ethernet link with routes to RFC 1918
    addresses that are currently otherwise unused on the host.

 * test-suite-mail-daemons

    a package that installs IMAP and SMTP daemons that serve/relay for
    two different domains within .test, which will be registered with
    test-suite-dns-daemon.  They should listen on the addresses set up
    by test-suite-network, using the expected SMTP/Submission/IMAP/IMAPS
    ports using a certificate from test-suite-ca, with three configured
    accounts on each of two domains.  The simplest possible
    authentication schemes will be used, with well-known passwords.
